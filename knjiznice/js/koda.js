window.addEventListener("load", function(){

var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */

var ena = "cc114ea9-5a62-4cee-9834-c145df9eeb7f";
var dva = "5e4979b6-d66a-41bd-9d5b-c8f977eed74c";
var tri = "247163cb-7c8e-4d59-9a26-956f069f8556";

var generiraj = function(){
    generirajPodatke(1, function(id){
        ena = id;
    });
    generirajPodatke(2, function(id) {
        dva = id;
    });
    generirajPodatke(3, function(id) {
        tri = id;
    });
}

document.querySelector("#generirajPodatke").addEventListener("click", generiraj);

var prvi = function(){
    branje(ena);
}
var drugi = function(){
    branje(dva);
}
var tretji = function(){
    branje(tri);
}
var specific = function(){
    var id = document.querySelector("#idPacienta").value;
    branje(id);
}
var branje = function(str){
    pridobiPodatke(str, function(podatki, ime){
        if(podatki==1){
            alert("Ni podatkov");
        }
        else if(podatki==2){
            alert("Prišlo je do napake");
        }
        else{
            var visina = podatki[1]*podatki[1]/10000;
            var bmi = podatki[0]/visina;
            console.log(bmi);
            izrisi(podatki[0], podatki[1], ime);
            pridobiWiki();
        }
    });
}
document.querySelector("#pac1").addEventListener("click", prvi);
document.querySelector("#pac2").addEventListener("click", drugi);
document.querySelector("#pac3").addEventListener("click", tretji);
document.querySelector("#poizvedba").addEventListener("click", specific);

function generirajPodatke(stPacienta, callback) {
  var ehrId;
  if(stPacienta==1){
      $.ajaxSetup({
          headers:{"Ehr-Session": getSessionId()}
      });
      $.ajax({
         url: baseUrl +"/ehr",
        type: "POST",
         success: function(data){
            ehrId = data.ehrId;
            var partyData = {
                firstNames: "Mary",
                lastNames: "Wilkinson",
                dateOfBirth: "1982-07-18",
                partyAdditionalInfo: [
                    {
                        key: "ehrId",
                        value: ehrId
                    }
                ]
            };
            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(partyData),
                success: function (party) {
                    if (party.action == 'CREATE') {
                    }
                },
            });
            var queryParams = {
                    "ehrId": ehrId,
                    templateId: 'Vital Signs',
                    format: 'FLAT',
                    committer: 'Belinda Nurse'
            }
            var compositionData = {
            "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": "8000-05-08T11:40:00Z",
		    "vital_signs/height_length/any_event/body_height_length": 165,
		    "vital_signs/body_weight/any_event/body_weight": 50,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": 36,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": 140,
		    "vital_signs/blood_pressure/any_event/diastolic": 90,
            "vital_signs/indirect_oximetry:0/spo2|numerator": 95
            };
            $.ajax({
                url: baseUrl + "/composition?" + $.param(queryParams),
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(compositionData),
                success: function (res) {
                    callback(ehrId);
                }
            });
         }
      });
  }
  if(stPacienta==2){
    $.ajaxSetup({
          headers:{"Ehr-Session": getSessionId()}
      });
      $.ajax({
         url: baseUrl +"/ehr",
        type: "POST",
         success: function(data){
            var ehrId = data.ehrId;
            var partyData = {
                firstNames: "Joe",
                lastNames: "Smith",
                dateOfBirth: "1945-11-26",
                partyAdditionalInfo: [
                    {
                        key: "ehrId",
                        value: ehrId
                    }
                ]
            };
            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(partyData),
                success: function (party) {
                    if (party.action == 'CREATE') {
                    }
                },
            });
            var queryParams = {
                    "ehrId": ehrId,
                    templateId: 'Vital Signs',
                    format: 'FLAT',
                    committer: 'Belinda Nurse'
            }
            var compositionData = {
                "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": "8000-05-08T11:40:00Z",
		    "vital_signs/height_length/any_event/body_height_length": 178,
		    "vital_signs/body_weight/any_event/body_weight": 78,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": 35,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": 110,
		    "vital_signs/blood_pressure/any_event/diastolic": 70,
            "vital_signs/indirect_oximetry:0/spo2|numerator": 90
            };
            $.ajax({
                url: baseUrl + "/composition?" + $.param(queryParams),
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(compositionData),
                success: function (res) {
                    callback(ehrId);
                }
            });
         }
      });
  }
  if(stPacienta==3){
      $.ajaxSetup({
          headers:{"Ehr-Session": getSessionId()}
      });
      $.ajax({
         url: baseUrl +"/ehr",
        type: "POST",
         success: function(data){
            ehrId = data.ehrId;
            var partyData = {
                firstNames: "Robin",
                lastNames: "Dabank",
                dateOfBirth: "2002-04-12",
                partyAdditionalInfo: [
                    {
                        key: "ehrId",
                        value: ehrId
                    }
                ]
            };
            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(partyData),
                success: function (party) {
                    if (party.action == 'CREATE') {
                        
                    }
                }
            });
            var queryParams = {
                    "ehrId": ehrId,
                    templateId: 'Vital Signs',
                    format: 'FLAT',
                    committer: 'Belinda Nurse'
            }
            var compositionData = {
                "ctx/language": "en",
		        "ctx/territory": "SI",
		        "ctx/time": "8000-05-08T11:40:00Z",
    		    "vital_signs/height_length/any_event/body_height_length": 173,
	    	    "vital_signs/body_weight/any_event/body_weight": 63,
	    	   	"vital_signs/body_temperature/any_event/temperature|magnitude": 36.5,
	    	    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
	    	    "vital_signs/blood_pressure/any_event/systolic": 120,
	    	    "vital_signs/blood_pressure/any_event/diastolic": 80,
                "vital_signs/indirect_oximetry:0/spo2|numerator": 100
            };
            $.ajax({
                url: baseUrl + "/composition?" + $.param(queryParams),
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(compositionData),
                success: function (res) {
                    callback(ehrId);
                }
            });
         }
      });
  }
}

function pridobiPodatke(idPacienta, callback){
    $.ajax({
    url: baseUrl + "/demographics/ehr/" + idPacienta + "/party",
    type: 'GET',
    headers: {
        "Ehr-Session": getSessionId()
    },
    success: function (data) {
        var party = data.party;
        var ime = party.firstNames+ " "+ party.lastNames;
        $.ajax({
  		    url: baseUrl + "/view/" + idPacienta + "/" + "weight",
		    type: 'GET',
		    headers: {"Ehr-Session": getSessionId()},
			success: function (res) {
				if (res.length > 0) {
				    var podatki = [0, 0];
					podatki[0]=res[0].weight;
					$.ajax({
					    url: baseUrl + "/view/" + idPacienta + "/" + "height",
		                type: 'GET',
		                headers: {"Ehr-Session": getSessionId()},
		                success: function(res){
		                    if(res.length>0){
		                        podatki[1] = res[0].height;
		                        callback(podatki, ime);
		                    }
		                    else{
		                        callback(1);
		                    }
		                },
		                error: function(){
		                    callback(2);
		                }
					});
			    }
			    else {
			        callback(1);
				}
			},
			error: function(){
			    callback(2);
			}
        });
    }
    });
}

function pridobiWiki(){
    $.ajax({
        type: "GET",
        url: "https://sl.wikipedia.org/w/api.php?action=parse&format=json&prop=text&section=0&page=Indeks_telesne_mase&callback=?",
        contentType: "application/json; charset=utf-8",
        async: false,
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            var markup = data.parse.text["*"];
            var blurb = $('<div></div>').html(markup);
            $('#podatki').html($(blurb).find('p'));
        },
        error: function (errorMessage) {
        }
    });
}

function izrisi(teza, visina, oseba){
    var ctx = document.getElementById("myChart").getContext('2d');
    var v = visina*visina/10000;
    var bmi = teza/v;
    var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [oseba+", "+teza+" kg, "+visina+" cm", "Povprečen BMI"],
        datasets: [{
            label: "BMI",
            data: [bmi, 22],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
}

});